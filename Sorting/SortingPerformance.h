#ifndef SORTING_PERFORMANCE_SORTING_PERFORMANCE_H

#define SORTING_PERFORMANCE_SORTING_PERFORMANCE_H

#include <iostream>
#include <string>
#include <ctime>

using std::string;

template<typename T>
class SortingPerformance {
private:

	T *_performanceTestingData;
	int _testingDataCounter;
public:
	SortingPerformance(T performanceTestingData[], int count) 
	{
		_performanceTestingData = performanceTestingData;
		_testingDataCounter = count;
	}

	~SortingPerformance() 
	{
		delete[] _performanceTestingData;
		_testingDataCounter = 0;
	}

	
	void testing(string sortName, void(*sorting)(T[], int))
	{

		clock_t startTime = clock();
		std::cout << startTime << std::endl;
		sorting(_performanceTestingData, _testingDataCounter);
		clock_t endTime = clock();
		std::cout << endTime << std::endl;

		std::cout << sortName << " : " << double (endTime - startTime) / CLOCKS_PER_SEC<< "s" << std::endl;
	}
};


template<typename T>
void selectionSortExternal(T externalData[], int elementCounter)
{
	if (1 >= elementCounter) {
		return;
	}

	for (int currentIndex = 0; currentIndex < elementCounter; currentIndex++) {

		int minimumIndex = currentIndex;

		for (int nextIndex = currentIndex + 1; nextIndex < elementCounter; nextIndex++) {
			if (externalData[minimumIndex] > externalData[nextIndex]) {
				minimumIndex = nextIndex;
			}
		}

		std::swap(externalData[minimumIndex], externalData[currentIndex]);
	}
}

#endif // !SORTING_PERFORMANCE_SORTING_PERFORMANCE_H
