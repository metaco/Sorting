// Sorting.cpp: 定义应用程序的入口点。
//

#include "Sorting.h"
#include <assert.h>

Sorting::Sorting(int data[], int elementCounter)
{
	assert(0 != elementCounter);

	_data = new int[elementCounter];

	for (int dataLocationIndex = 0; dataLocationIndex < elementCounter; dataLocationIndex++) {
		_data[dataLocationIndex] = data[dataLocationIndex];
	}
	// 内存复制出现的问题 是由 char * 指针引起的
	// memcpy(_data, data, elementCounter);

	_elementCounter = elementCounter;
}


Sorting::~Sorting()
{
	delete[] _data;
}


void Sorting::traversal()
{
	if (0 >= _elementCounter) {
		return ;
	}

	for (int dataIndex = 0; dataIndex < _elementCounter; dataIndex++) {
		std::cout << _data[dataIndex] << std::endl;
	}
}

void Sorting::selectionSort()
{
	if (1 >= _elementCounter) {
		return ;
	}

	for (int currentIndex = 0; currentIndex < _elementCounter; currentIndex++) {
		// 定义当前的位置为最小位标记
		int minimumIndex = currentIndex;
		// 以当前的元素为比较对象交替索引位置
		for (int nextIndex = currentIndex + 1; nextIndex < _elementCounter; nextIndex++) {
			if (_data[minimumIndex] > _data[nextIndex]) {
				minimumIndex = nextIndex;
			}
		}

		std::swap(_data[minimumIndex], _data[currentIndex]);
	}
}


void Sorting::insertionSort() {
	if (1 >= _elementCounter) {
		return ;
	}

	for (int searchIndex = 0; searchIndex < _elementCounter; searchIndex++) {
		// 插入排序开始从当前向后的索引位向后递减依次比较
		for (int currentSearchIndex = searchIndex + 1; currentSearchIndex >= 0 ;
		currentSearchIndex--) {
			// 当前的元素比之前的元素小，交换位置
			if (_data[currentSearchIndex] < _data[ currentSearchIndex - 1 ]) {
				std::swap(_data[currentSearchIndex], _data[currentSearchIndex - 1]);
			} else {
				break;
			}
		}
	}
}


void Sorting::mergeSort() {
	_mergeSorting(_data, 0, _elementCounter - 1);
}

/**
 * 递归使用归并排序，对dataStorage[beginIndex...endIndex] 范围排序
 * @param dataStorage
 * @param beginIndex
 * @param endIndex
 */
void Sorting::_mergeSorting(int *dataStorage, int beginIndex, int endIndex) {
	if (beginIndex >= endIndex) {
		// 当前处理的数据集为空或1
		return ;
	}

	int middleIndex = (beginIndex + endIndex) / 2 ; // 此操作可能会造成溢出，beginIndex,endIndex 过大
	// 进行子排序
	_mergeSorting(dataStorage, beginIndex, middleIndex);
	_mergeSorting(dataStorage, middleIndex + 1, endIndex);

	// merge 改进
	if ( dataStorage[middleIndex] > dataStorage[middleIndex + 1]) {
		_mergeData(dataStorage, beginIndex, middleIndex, endIndex);
	}
}

/**
 * 将dataStorage[beginIndex..middleIndex]
 * 		和 【middleIndex+ 1, .... endIndex】 合并
 * @param dataStorage
 * @param beginIndex
 * @param middleIndex
 * @param endIndex
 */
void Sorting::_mergeData(int *dataStorage, int beginIndex, int middleIndex, int endIndex) {
    // 辅助空间算法
	int auxiliaryStorage[endIndex - beginIndex + 1];

	for (int currentIndex = beginIndex; currentIndex <= endIndex; currentIndex++) {
		// 需要从0 索引位置开始存储数据，指针位置是减去起始位置的
		auxiliaryStorage[currentIndex - beginIndex] = dataStorage[currentIndex];
	}
	// 归并的两部分，元素索引起始位置从两段索引位置开始计算
	int part1beginIndex = beginIndex, part2StartIndex = middleIndex + 1;

	// 合并存储的空间数据位置[beginIndex....endIndex];
	for (int traversalMergeIndex = beginIndex; traversalMergeIndex <= endIndex;
		 traversalMergeIndex++) {
		// 存在数组越界的情况
		if (part1beginIndex > middleIndex) { // 说明数组第一部分已经越界中间位置
			dataStorage[traversalMergeIndex] = auxiliaryStorage[part2StartIndex - beginIndex];
			part2StartIndex++;
		} else if (part2StartIndex > endIndex) { // 说明数组归并的第二部分已经越过最后的位置
			dataStorage[traversalMergeIndex] =  auxiliaryStorage[part1beginIndex - beginIndex];
			part1beginIndex++;
		} else if (auxiliaryStorage[part1beginIndex - beginIndex] < auxiliaryStorage[part2StartIndex - beginIndex]) {
			// 比较两个段起始数据位置的索引项
			// 数据可能会存在越界的情况
			dataStorage[traversalMergeIndex] = auxiliaryStorage[part1beginIndex - beginIndex];
			// 分段的起始位置前移
			part1beginIndex++;
		} else {
			dataStorage[traversalMergeIndex] = auxiliaryStorage[part2StartIndex - beginIndex];
			part2StartIndex++;
		}
	}

}
/*
 * 对于一个近乎有序的随机序列，该插入排序算速度极快
*/
void Sorting::insertionSortOptimize()
{
	if (1 >= _elementCounter) {
		return;
	}

	for (int searchIndex = 1; searchIndex < _elementCounter; searchIndex++) {

		// 寻找元素 searchIndex 索引位置合适插入的位置
		int elementInsertionStorage = _data[searchIndex];
		int insertionIndex; // 保存元素 elementInsertion 插入位置

		// 一旦找到合适的位置提前终止循环条件
		for (insertionIndex = searchIndex ; insertionIndex > 0; insertionIndex--) {
			// 当前的元素比之前的元素小，交换位置
			if (_data[insertionIndex - 1] < elementInsertionStorage) {
				_data[insertionIndex] = _data[insertionIndex - 1];
			}
			else {
				break;
			}
		}
		_data[insertionIndex] = elementInsertionStorage;
	}
}

