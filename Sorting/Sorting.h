﻿// Sorting.h: 标准系统包含文件的包含文件
// 或项目特定的包含文件。

#pragma once

#include <iostream>

#ifndef SORTING_SORTING_H
#define SORTING_SORTING_H

class Sorting {
private:
	int *_data;
	int _elementCounter;
	void _mergeSorting(int dataStorage[], int beginIndex, int endIndex);
	void _mergeData(int dataStorage[], int beginIndex, int middleIndex, int endIndex);

public:
	Sorting(int data[], int elementCounter);
	~Sorting();

	void traversal();
	// 选择排序
	void selectionSort();
	// 插入排序
	void insertionSort();
    // 插入排序改进算法
    void insertionSortOptimize();
	// 归并排序
	void mergeSort();


};

#endif // !SORTING_SORTING_H

// TODO: 在此处引用程序需要的其他标头。
