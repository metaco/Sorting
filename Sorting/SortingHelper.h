#ifndef SORTINGHELPER_SORTINGHELPER_H

#include <iostream>
#include <ctime>

namespace SortingHelper {
	/* 
	 *@param int counter  数量
	 *@param int rangeL   左边范围L
	 *@param int rangeR   左边范围R
	 */
	int * generateRandomArray(int counter, int rangeL, int rangeR) {

		if (rangeR < rangeL) {
			return NULL;
		}

		int *generateArray = new int[counter];

		srand(time(NULL));

		for (int index = 0; index < counter; index++) {
			generateArray[index] = rand() % (rangeR - rangeL + 1) + rangeL; 
		}

		return generateArray;
	}

	// 生成一个近乎有序的数组
	int *generateNearlyOrderedArray(int elementCount, int swapTimes)
	{
		int *generateArray = new int[elementCount];

		for (int index = 0; index < elementCount; index++) {
			generateArray[index] = index;
		}

		srand(time(NULL));
		// 随机交换几次数组元素的位置，生成一个近乎有序的排序数组
		for (int i = 0; i < swapTimes; i++) {
			int posX = rand() % elementCount;
			int posY = rand() % elementCount;

			std::swap(generateArray[posX], generateArray[posY]);
		}

		return generateArray;
	}
}

#endif // !SORTINGHELPER_SORTINGHELPER_H
