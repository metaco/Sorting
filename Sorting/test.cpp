#include "Sorting.h"
#include "SortingHelper.h"
#include "SortingPerformance.h"

#include <string.h>
#include <string>

int main(int argc, const char *argv[]) 
{
	
	int *sortingData = SortingHelper::generateRandomArray(10000, 48, 1678);
	int *nearlySortingData = SortingHelper::generateNearlyOrderedArray(1000, 4);

	if (NULL == sortingData) {
		return -1;
	}

	SortingPerformance<int> sortingPerformance(nearlySortingData, 10);
	Sorting sorting(nearlySortingData, 1000);
	
	// sortingPerformance.testing("selectionSort", selectionSortExternal);
	// sorting.insertionSort();
	sorting.mergeSort();
	// sorting.insertionSort();
	// sorting.insertionSortOptimize();
	// sorting.selectionSort();
	sorting.traversal();
	
	return 0;

}